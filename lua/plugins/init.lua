-- This is based on kickstart.nvim
-- https://github.com/nvim-lua/kickstart.nvim/blob/master/init.lua


local function install_lazyvim()
	-- Install package manager
	--    https://github.com/folke/lazy.nvim
	--    `:help lazy.nvim.txt` for more info
	local lazypath = vim.fn.stdpath 'data' .. '/lazy/lazy.nvim'
	if not vim.loop.fs_stat(lazypath) then
		print("installing lazy vim..")
		vim.fn.system {
			'git',
			'clone',
			'--filter=blob:none',
			'https://github.com/folke/lazy.nvim.git',
			'--branch=stable', -- latest stable release
			lazypath,
		}
	end
	vim.opt.rtp:prepend(lazypath)
end

local function get_tpope()
	return {
		-- Git related plugins
		'tpope/vim-fugitive',
		'tpope/vim-rhubarb',
		-- Detect tabstop and shiftwidth automatically
		'tpope/vim-sleuth',
	}
end

local function get_lsp_zero()
	return {
		'VonHeikemen/lsp-zero.nvim',
		branch = 'v2.x',
		dependencies = {
			-- LSP Support
			{ 'neovim/nvim-lspconfig' }, -- Required
			{ 'williamboman/mason.nvim' }, -- Optional
			{ 'williamboman/mason-lspconfig.nvim' }, -- Optional

			-- Autocompletion
			{ 'hrsh7th/nvim-cmp' }, -- Required
			{ 'hrsh7th/cmp-nvim-lsp' }, -- Required
			{ 'L3MON4D3/LuaSnip' }, -- Required
			-- Useful status updates for LSP
			-- NOTE: `opts = {}` is the same as calling `require('fidget').setup({})`
			{ 'j-hui/fidget.nvim',                tag = 'legacy', opts = {} },
			-- Additional lua configuration, makes nvim stuff amazing!
			{ 'folke/neodev.nvim' },
			{ 'jose-elias-alvarez/null-ls.nvim' },
			{"jay-babu/mason-null-ls.nvim"},
		},
	}
end


local function get_autocomplete()
	return {
		-- Autocompletion
		'hrsh7th/nvim-cmp',
		dependencies = {
			-- Snippet Engine & its associated nvim-cmp source
			'L3MON4D3/LuaSnip',
			'saadparwaiz1/cmp_luasnip',

			-- Adds LSP completion capabilities
			'hrsh7th/cmp-nvim-lsp',

			-- Adds a number of user-friendly snippets
			'rafamadriz/friendly-snippets',
		}
	}
end


local function get_which_key()
	-- Useful plugin to show you pending keybinds.
	return { 'folke/which-key.nvim', opts = {} },
	    {
		    -- Adds git releated signs to the gutter, as well as utilities for managing changes
		    'lewis6991/gitsigns.nvim',
		    opts = {
			    -- See `:help gitsigns.txt`
			    signs = {
				    add = { text = '+' },
				    change = { text = '~' },
				    delete = { text = '_' },
				    topdelete = { text = '‾' },
				    changedelete = { text = '~' },
			    },
			    on_attach = function(bufnr)
				    vim.keymap.set('n', '<leader>gp', require('gitsigns').prev_hunk,
					    { buffer = bufnr, desc = '[G]o to [P]revious Hunk' })
				    vim.keymap.set('n', '<leader>gn', require('gitsigns').next_hunk,
					    { buffer = bufnr, desc = '[G]o to [N]ext Hunk' })
				    vim.keymap.set('n', '<leader>ph', require('gitsigns').preview_hunk,
					    { buffer = bufnr, desc = '[P]review [H]unk' })
			    end,
		    },
	    }
end

local function get_misc()
	return { {

		-- Set lualine as statusline
		'nvim-lualine/lualine.nvim',
		-- See `:help lualine.txt`
		opts = {
			options = {
				icons_enabled = false,
				theme = 'onedark',
				component_separators = '|',
				section_separators = '',
			},
		},
	},

		{
			-- Add indentation guides even on blank lines
			'lukas-reineke/indent-blankline.nvim',
			-- Enable `lukas-reineke/indent-blankline.nvim`
			-- See `:help indent_blankline.txt`
			opts = {
				char = '┊',
				show_trailing_blankline_indent = false,
			},
		},

		-- this is to surround with brackets/quotes
		{'tpope/vim-surround'}
	


	}
end



local function setup_lsp_zero()
	local lsp = require('lsp-zero').preset({})

	lsp.on_attach(function(client, bufnr)
		-- see :help lsp-zero-keybindings
		-- to learn the available actions
		lsp.default_keymaps({ buffer = bufnr })
	end)

	-- (Optional) Configure lua language server for neovim

	lsp.setup()
end

local function get_telescope()
	return {
		{
			'nvim-telescope/telescope.nvim',
			branch = '0.1.x',
			dependencies = { 'nvim-lua/plenary.nvim',
			}
		},

		{
			'nvim-telescope/telescope-fzf-native.nvim',
			-- NOTE: If you are having trouble with this installation,
			--       refer to the README for telescope-fzf-native for more instructions.
			build = 'make',
			cond = function()
				return vim.fn.executable 'make' == 1
			end,
		},


	}
end

local function get_treesitter()
	return {
		{
			-- Highlight, edit, and navigate code
			'nvim-treesitter/nvim-treesitter',
			dependencies = {
				'nvim-treesitter/nvim-treesitter-textobjects',
			},
			build = ':TSUpdate',
		},

	}
end
-- Enable the following language servers
--  Feel free to add/remove any LSPs that you want here. They will automatically be installed.
--
--  Add any additional override configuration in the following tables. They will be passed to
--  the `settings` field of the server config. You must look up that documentation yourself.
--
--  If you want to override the default filetypes that your language server will attach to you can
--  define the property 'filetypes' to the map in question.
local servers = {
	clangd = {},
	gopls = {},
	pyright = {},
	rust_analyzer = {},
	tsserver = {},
	terraformls = {},
	tflint = {},
	html = { filetypes = { 'html', 'twig', 'hbs' } },

	lua_ls = {
		Lua = {
			workspace = { checkThirdParty = false },
			telemetry = { enable = false },
		},
	},
}

local function configure_lsp_server()
	local mason_lspconfig = require 'mason-lspconfig'

	mason_lspconfig.setup {
		ensure_installed = vim.tbl_keys(servers),
	}

	mason_lspconfig.setup_handlers {
		function(server_name)
			require('lspconfig')[server_name].setup {
				capabilities = capabilities,
				on_attach = on_attach,
				settings = servers[server_name],
				filetypes = (servers[server_name] or {}).filetypes,
			}
		end
	}
end

local function get_code_plugins()
	return {
		"nvim-neotest/neotest",
		dependencies = {
			"nvim-lua/plenary.nvim",
			"nvim-treesitter/nvim-treesitter",
			"antoinemadec/FixCursorHold.nvim",
		},
		"nvim-neotest/neotest-python",
		"tpope/vim-commentary",
		"tpope/vim-projectionist",
		"andythigpen/nvim-coverage",

		-- markdown 
		{"ellisonleao/glow.nvim", config = true, cmd = "Glow"}

	}
end


local function get_theme()
	return {
		'maxmx03/dracula.nvim',
		lazy = false, -- make sure we load this during startup if it is your main colorscheme
		priority = 1000, -- make sure to load this before all the other start plugins
		config = function()
			local dracula = require 'dracula'

			dracula.setup()

			vim.cmd.colorscheme 'dracula'
		end
	}
end

local function get_theme_opts()
	return {
		install = {
			missing = true,
			colorscheme = { 'dracula' }
		}
	}
end


local function get_plugin_opts()
	return { get_theme_opts() }
end



local function install_plugins(opts)
	require('lazy').setup({ get_tpope(),
		get_lsp_zero(),
		get_autocomplete(),
		get_which_key(),
		get_telescope(),
		get_treesitter(),
		get_misc(),
		get_theme(),
		get_code_plugins()
	}, opts)
end


install_lazyvim()
install_plugins(get_plugin_opts())
setup_lsp_zero()
-- configures lsp server
configure_lsp_server()


require("plugins.options")
