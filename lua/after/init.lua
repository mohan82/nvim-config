local mason_ls = require("mason-null-ls")



mason_ls.setup({
  ensure_installed = { "black", "ruff" },
  automatic_installation = true,

})

local null_ls = require("null-ls")

null_ls.setup({
  sources = {
    null_ls.builtins.formatting.black,
    null_ls.builtins.diagnostics.ruff,
  },
})


local neotest = require('neotest')


neotest.setup({
  adapters = {
    require("neotest-python")({
      dap = {
        justMyCode = false,
        console = "integratedTerminal",
      },
      python = "/usr/local/Caskroom/miniconda/base/bin/python",
      args = { "--cov","--log-level", "DEBUG", "--quiet" },
      runner = "pytest",
    })
  }
})


require("coverage").setup({
  commands = true, -- create commands
  highlights = {
    -- customize highlight groups created by the plugin
    covered = { fg = "#C3E88D" }, -- supports style, fg, bg, sp (see :h highlight-gui)
    uncovered = { fg = "#F07178" },
  },
  signs = {
    -- use your own highlight groups or text markers
    covered = { hl = "CoverageCovered", text = "▎" },
    uncovered = { hl = "CoverageUncovered", text = "▎" },
  },
  summary = {
    -- customize the summary pop-up
    min_coverage = 80.0, -- minimum coverage threshold (used for highlighting)
  },
  lang = {
    -- customize language specific settings
  },
})
