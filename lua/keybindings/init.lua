local function set_global_keymap()
	vim.g.mapleader = " "
	vim.g.maplocalleader = " "

	--- Lets map buffers to leader b
	vim.keymap.set("n", "<leader>b", vim.cmd.buffers)

	-- Keymaps for better default experience
	-- See `:help vim.keymap.set()`
	vim.keymap.set({ 'n', 'v' }, '<Space>', '<Nop>', { silent = true })

	-- Remap for dealing with word wrap
	vim.keymap.set('n', 'k', "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
	vim.keymap.set('n', 'j', "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })

	-- [[ Highlight on yank ]]
	-- See `:help vim.highlight.on_yank()`
	local highlight_group = vim.api.nvim_create_augroup('YankHighlight', { clear = true })

	vim.api.nvim_create_autocmd('TextYankPost', {
		callback = function()
			vim.highlight.on_yank()
		end,
		group = highlight_group,
		pattern = '*',
	})
	vim.keymap.set('n','<leader>r',function() vim.lsp.buf.rename() end)

end


local function set_autoformat()
	vim.keymap.set('n', "<C-f>",function()
		   vim.lsp.buf.format() 
		end )
end


local function neotest()
	local neotest = require('neotest')
	vim.keymap.set('n', "<C-t>",function()
		neotest.run.run(vim.fn.expand("%"))	
	end )
end 

set_global_keymap()
require("keybindings.telescope")


set_autoformat()
neotest()
